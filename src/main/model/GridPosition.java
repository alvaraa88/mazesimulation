package model;

public class GridPosition {
    private int row;
    private int col;

    public GridPosition(int row, int col){
        this.row = row;
        this.col = col;
    }

    /**------------------
     * GETTERS & SETTERS
     *-------------------*/

    public int getRow(){
        return row;
    }
    public int getCol(){
        return col;
    }
}
