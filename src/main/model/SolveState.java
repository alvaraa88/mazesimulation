package model;

import javafx.scene.paint.Color;

public enum SolveState {
    CURRENT {
        @Override
        public Color getColor() {
            return Color.PALEGREEN;
        }
    },
    VISITED {
        @Override
        public Color getColor() {
            return Color.YELLOW;
        }
    },
    START {
        @Override
        public Color getColor() {
            return Color.ORANGERED;
        }
    },
    END {
        @Override
        public Color getColor() {
            return Color.ORANGERED;
        }
    },
    UNVISITED {
        @Override
        public Color getColor() {
            return Color.LIGHTBLUE;
        }
    };
    abstract public Color getColor();
}
