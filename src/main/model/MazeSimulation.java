package model;

import generation.UniqueGeneration;
import javafx.scene.layout.Pane;
import solving.Astar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static gui.MazeController.*;

public class MazeSimulation {
    private Pane pane;
    private CellModel cell;
    private UniqueGeneration uniqueGeneration;
    private Astar astar;
    private GridPosition gridPosition;
    private HashMap<GridPosition, CellModel> gridMap;
    public MazeSimulation(Pane pane) {
        this.pane = pane;
        gridMap = new HashMap<>();
    }

    /**
     * ---------------------
     * Getters and Setters
     * ---------------------
     */
    public CellModel getGridMapCell(int row, int col) throws IllegalArgumentException {
        for (Map.Entry<GridPosition, CellModel> loopCell : gridMap.entrySet()) {
           CellModel cm = loopCell.getValue();

            if (cm.getThisRow() == row && cm.getThisCol() == col) {
                return cm;
            }
        }
        throw new IllegalArgumentException("No cell found at position (" + row + ", " + col + ")");
    }

    /**
     * ---------------------
     * Methods
     * ---------------------
     */

    public void generateGridPositions() {
        //generating grid positions
        for (int x = 0; x < mazeWidth; x++) {
            for (int y = 0; y < mazeHeight; y++) {
                gridPosition = new GridPosition(x, y);
                cell = new CellModel(gridPosition, MazeState.CLOSED, SolveState.UNVISITED, pane);
                gridMap.put(gridPosition, cell);
            }
        }
        getGridMapCell(0, 0).setMazeState();

    }

    public void generateMazePath() {
        CellModel starting_cell;
        getGridMapCell(0,0).setCellState(MazeState.CURRENT);
        starting_cell = getGridMapCell(0,0);
        uniqueGeneration = new UniqueGeneration(gridMap, starting_cell);

        for (Map.Entry<GridPosition, CellModel> loopCell : gridMap.entrySet()) {
            CellModel cm;
            cm = loopCell.getValue();

            uniqueGeneration.addMazeNeighbors(cm, cm.getThisRow(), cm.getThisCol());
        }
        draw();
    }
    public void generateMazeBuilding(){
        while (!uniqueGeneration.isMazeComplete()) {
            uniqueGeneration.randomMazeBuild();
        }
    }

    public void generateSolvingPath(){
        CellModel starting_cell = getGridMapCell(0, 0);
        //getGridMapCell(0,0).setSolvingState(SolveState.CURRENT);
        starting_cell.setSolvingState(SolveState.CURRENT);
        //starting_cell = getGridMapCell(0,0);

        Random random = new Random();
        int randomRowNumber = random.nextInt(mazeWidth - 1);
        int endCol = mazeHeight - 1;

        CellModel end_cell = getGridMapCell(randomRowNumber, endCol);
        //getGridMapCell(randomRowNumber, endCol).setSolvingState(SolveState.END);
        end_cell.setSolvingState(SolveState.END);

        astar = new Astar(uniqueGeneration.getMazeMapGrid(), starting_cell, end_cell);

        draw();
    }

    public void generateSolvingAlgorithm(){
        while (!astar.isSolvingComplete()) {
            astar.aStarSolvingAlgorithm();
        }
    }

    //add type of maze building movement here
    public void move() {
        if(!uniqueGeneration.isMazeComplete()){
            uniqueGeneration.randomMazeBuild();
        }
        if(uniqueGeneration.isMazeComplete()){
            astar.aStarSolvingAlgorithm();
        }
        System.out.println("no more steps.");
    }

    public void moveBuilding() {
        uniqueGeneration.randomMazeBuild();
        if(uniqueGeneration.isMazeComplete()){
            System.out.println("no more steps.");
        }
    }

    public void moveSolving() {
        astar.aStarSolvingAlgorithm();
        if(astar.isSolvingComplete()){
            System.out.println("no more steps.");
        }
    }
    public void draw() {
        for (Map.Entry<GridPosition, CellModel> loopCell : gridMap.entrySet()) {
            CellModel cm;
            cm = loopCell.getValue();

            cm.drawAll();
        }
    }
}
