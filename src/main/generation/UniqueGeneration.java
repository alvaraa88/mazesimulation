package generation;

import model.CellModel;
import model.GridPosition;
import model.MazeState;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static gui.MazeController.mazeHeight;
import static gui.MazeController.mazeWidth;

public class UniqueGeneration {
    private CellModel current_cell;
    private CellModel nextCurrentCell;
    private CellModel neighboring_cell;
    private ArrayList<CellModel> stack = new ArrayList<>();
    private HashMap<GridPosition, CellModel> gridMap;
    private boolean mazeComplete;

    public UniqueGeneration(HashMap<GridPosition, CellModel> gridMap, CellModel current_cell) {
        this.gridMap = gridMap;
        this.current_cell = current_cell;
    }

    /**
     * ---------------------
     * Getters and Setters
     * ---------------------
     */
    public void setMazeComplete(boolean mazeComplete) {
        this.mazeComplete = mazeComplete;
    }

    public boolean isMazeComplete() {
        return mazeComplete;
    }

    public HashMap<GridPosition, CellModel> getMazeMapGrid(){
        return gridMap;
    }

    public CellModel getMazeMapCell(int row, int col) {
        for (Map.Entry<GridPosition, CellModel> loopCell : gridMap.entrySet()) {
            CellModel cm;
            cm = loopCell.getValue();

            if (cm.getThisRow() == row && cm.getThisCol() == col) {
                return cm;
            }
        }
        System.out.println("ERROR IN: =UniqueGeneration= getMazeMapCell()");
        return null;
    }

    /**
     * ---------------------
     * METHODS
     * ---------------------
     */
    //algorithm to move to a random cell.
    public void randomMazeBuild() {
        //if current cell has unvisited neighbors
        if (unvisitedNeighbors(current_cell)) {

            //if neighbor is already VISITED add to the stack(this will give you a backtrace of cells)
            getMazeMapCell(current_cell.getThisRow(), current_cell.getThisCol()).setCellState(MazeState.VISITED);

            stack.add(getMazeMapCell(current_cell.getThisRow(), current_cell.getThisCol()));

            //pick a random Neighbor from current cell, set to visited
            nextCurrentCell = pickRandomNeighbor(getMazeMapCell(current_cell.getThisRow(), current_cell.getThisCol()));
            removeWalls(
                    getMazeMapCell(current_cell.getThisRow(), current_cell.getThisCol()),
                    getMazeMapCell(nextCurrentCell.getThisRow(), nextCurrentCell.getThisCol()));

            current_cell = nextCurrentCell;
            getMazeMapCell(current_cell.getThisRow(), current_cell.getThisCol()).setCellState(MazeState.CURRENT);

            drawMap();

            //else-if make sure the stack has something
        } else if (stack.size() > 0) {
            //grab from the stack and set to current cell to backtrace.
            getMazeMapCell(current_cell.getThisRow(), current_cell.getThisCol()).setCellState(MazeState.VISITED);

            nextCurrentCell = stack.get(stack.size() - 1);
            stack.remove(getMazeMapCell(nextCurrentCell.getThisRow(), nextCurrentCell.getThisCol()));
            removeWalls(
                    getMazeMapCell(current_cell.getThisRow(), current_cell.getThisCol()),
                    getMazeMapCell(nextCurrentCell.getThisRow(), nextCurrentCell.getThisCol()));
            current_cell = nextCurrentCell;
            getMazeMapCell(current_cell.getThisRow(), current_cell.getThisCol()).setCellState(MazeState.CURRENT);

            drawMap();
        } else {
            System.out.println("Maze done.");
            setMazeComplete(true);
            drawMap();

            //need to stop start loop here.
            //trigger stop button.
        }
    }

    private CellModel pickRandomNeighbor(CellModel current) {

        //get random block from neighbors
        neighboring_cell = getMazeMapCell(current.getThisRow(), current.getThisCol())
                .getNeighbors().get((int) Math.floor(Math.random() * current.getNeighbors().size()));

        //if neighboring_cell has been visited, remove from neighboring array.
        while (neighboring_cell.getCellState().visited()) {
            getMazeMapCell(neighboring_cell.getThisRow(), neighboring_cell.getThisCol())
                    .getNeighbors().remove(neighboring_cell);

            //pick random nextNeighbor again and set the neighboring_cell to visited and repeat process
            neighboring_cell = getMazeMapCell(current.getThisRow(), current.getThisCol())
                    .getNeighbors().get((int) Math.floor(Math.random() * current.getNeighbors().size()));
            //infinite loop possible here IF you call method after all cells have been visited.

        }
        getMazeMapCell(neighboring_cell.getThisRow(), neighboring_cell.getThisCol()).getNeighbors().remove(neighboring_cell);
        return neighboring_cell;
    }

    private boolean unvisitedNeighbors(CellModel current) {
        for (CellModel cellModel : getMazeMapCell(current.getThisRow(), current.getThisCol()).getNeighbors()) {
            if (!(cellModel.getCellState().visited())) {
                return true;
            }
        }
        return false;
    }

    private void removeWalls(CellModel current, CellModel neighborCell) {
        int rowWalls = current.getThisRow() - neighborCell.getThisRow();
        int colWalls = current.getThisCol() - neighborCell.getThisCol();

        //East-[1] and West-[3]
        if (rowWalls == -1) {
            //RIGHT WALL
            getMazeMapCell(current.getThisRow(), current.getThisCol()).setEastWall(false);
            getMazeMapCell(neighborCell.getThisRow(), neighborCell.getThisCol()).setWestWall(false);
        } else if (rowWalls == 1) {
            //LEFT WALL
            getMazeMapCell(current.getThisRow(), current.getThisCol()).setWestWall(false);
            getMazeMapCell(neighborCell.getThisRow(), neighborCell.getThisCol()).setEastWall(false);
        }
        //North-[0] and South-[2]
        if (colWalls == -1) {
            //DOWNWARD WALL
            getMazeMapCell(current.getThisRow(), current.getThisCol()).setSouthWall(false);
            getMazeMapCell(neighborCell.getThisRow(), neighborCell.getThisCol()).setNorthWall(false);
        } else if (colWalls == 1) {
            //UPWARD WALL
            getMazeMapCell(current.getThisRow(), current.getThisCol()).setNorthWall(false);
            getMazeMapCell(neighborCell.getThisRow(), neighborCell.getThisCol()).setSouthWall(false);
        }
    }
    public void addMazeNeighbors(CellModel cell, int thisRow, int thisCol) {
        //add LOWER BOUND.
        if (thisCol > 0) {
            //add upper cell
            cell.getNeighbors().add(getMazeMapCell(thisRow, thisCol - 1));
        }
        //add TOP BOUND.
        if (thisCol < mazeHeight - 1) {
            //add lower cell
            cell.getNeighbors().add(getMazeMapCell(thisRow, thisCol + 1));
        }
        //add RIGHT BOUND
        if (thisRow > 0) {
            //add left cell
            cell.getNeighbors().add(getMazeMapCell(thisRow - 1, thisCol));
        }
        //add LEFT BOUND.
        if (thisRow < mazeWidth - 1) {
            //add right
            cell.getNeighbors().add(getMazeMapCell(thisRow + 1, thisCol));
        }
    }
    public void drawMap() {
        for (Map.Entry<GridPosition, CellModel> cellLoop : gridMap.entrySet()) {
            CellModel cm;
            cm = cellLoop.getValue();

            cm.drawMazePath();
        }
    }
    public void drawAll(){
        drawMap();
    }
}
