# MazeSimulation

Maze Simulation based on:
https://en.wikipedia.org/wiki/Maze_generation_algorithm
and
https://en.wikipedia.org/wiki/Pathfinding

Hello Thanks for checking out my Maze Simulation project.
Here are some of the main points I would like to point out as the objectivfe of this project.
1) Processing Fundamentals.
2) Object Oriented Design.
3) Maze Building Algorithms.
4) Maze Solving Algorithms.

## Getting started:
This project shows design of intermediate programming using processing techniques and algorithm implementations. 

### how it works:
* This project has 4 designed pages.
  1) Starting page - this page contains implementation of maze generation and maze solving algorithms. while also having 3 buttons taking you to different pages.
     * Generation - shows step by step maze generation simulation algorithm.
     * Solver - shows the step by step solving simulation algorithm.
     * Simulation - shows the final result of both the generation and solving algorithms in tandem.


## About Project
* This program consists of a few ideas, but mainly it simulates algorithms.
2 types of algorithms specifically: Maze Generation Algorithms and Maze Solving Algorithms. thus this project can thus be seen in 2 parts.
  * Maze generation.
  * Maze solving.
>The **MazeApp** is responsible for combining all the model classes and
> forming a working simulation via a GUI.

>The **Maze Simulation** is responsible for the simulation of either the Maze generation / Maze Solver.

>The **Generation Factory** is responsible for distribution of Maze Building algorithms.

>The **Solving Factory** is responsible for distribution of Maze Solving algorithms.

>The **gui package** contains main class and classes containing GUI elements

>The **model package** contains the basic building classes of a maze.

>The **solving package** contains the maze building classes.

> >The **generation package** contains the maze solving classes.


### Author and website:
1) [Alexander Alvara](https://alexanderalvara.com/resume)
2) [Gitlab](https://gitlab.com/alvaraa88/mazesimulation)
3) [LinkedIn](https://www.linkedin.com/in/alexander-alvara-4132a3a7/)
